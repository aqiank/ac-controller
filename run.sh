#!/bin/sh

cargo run -- \
-m porcupine_params.pv \
-k air\ con_linux.ppn \
-k air\ con\ off_linux.ppn \
-k air\ con\ on_linux.ppn \
-k too\ hot_linux.ppn \
-k too\ cold_linux.ppn
