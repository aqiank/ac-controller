extern crate serialport;

// std
use std::io::Write;
use std::time::{SystemTime, Duration};
use std::thread;
use std::sync::mpsc::channel;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

// chan
#[macro_use]
extern crate chan;

// chan-signal
extern crate chan_signal;

// clap
extern crate clap;
use clap::{App, Arg};

// cpal
extern crate cpal;

// porcupine-sys
extern crate porcupine_sys;
use porcupine_sys as pv;

// sample
extern crate sample;
use sample::Signal;

// serialport
use serialport::{SerialPortType, open};

const TOGGLE: u8 = 10;
const POWER_OFF: u8 = 0xA1;
const POWER_ON: u8 = 0xA2;
const TOO_COLD: u8 = 0xA3;
const TOO_HOT: u8 = 0xA4;

fn main() {
    let mut port_name = String::new();
    
    if let Ok(ports) = serialport::available_ports() {
        match ports.len() {
            0 => println!("No ports found."),
            1 => println!("Found 1 port:"),
            n => println!("Found {} ports:", n),
        };
        for p in ports {
            println!("  {}", p.port_name);
            match p.port_type {
                SerialPortType::UsbPort(info) => {
                    println!("    Type: USB");
                    println!("    VID:{:04x} PID:{:04x}", info.vid, info.pid);
                    println!(
                        "     Serial Number: {}",
                        info.serial_number.as_ref().map_or("", String::as_str)
                    );
                    println!(
                        "      Manufacturer: {}",
                        info.manufacturer.as_ref().map_or("", String::as_str)
                    );
                    println!(
                        "           Product: {}",
                        info.product.as_ref().map_or("", String::as_str)
                    );

                    if info.vid == 0x2341 && info.pid == 0x0043 {
                        port_name = p.port_name;
                        break;
                    }
                }
                SerialPortType::BluetoothPort => {
                    println!("    Type: Bluetooth");
                }
                SerialPortType::PciPort => {
                    println!("    Type: PCI");
                }
                SerialPortType::Unknown => {
                    println!("    Type: Unknown");
                }
            }
        }

        if port_name.is_empty() {
            println!("No Arduino port was found");
            return;
        }
    } else {
        print!("Error listing serial ports");
        return;
    };

    // Open the first serialport available.
    let serialport = open(&port_name)
        .expect("Failed to open serial port");

    // Clone the port
    let mut clone = serialport.try_clone().expect("Failed to clone");

    // Create channel for sending/receiving commands
    let (tx, rx) = channel();

    // Setup thread for sending command received from the main thread
    thread::spawn(move || loop {
        loop {
            if let Ok(b) = rx.recv() {
                clone
                    .write(&[b])
                    .expect("Failed to write to serial port");
            }
            thread::sleep(Duration::from_millis(1000));
        }
    });

    // Setup command-line options
    let matches = App::new("AC Controller")
        .version("0.1.0")
        .about("This is a program for controlling Air Conditioners using voice")
        .arg(
            Arg::with_name("model-file-path")
                .short("m")
                .long("model-file-path")
                .value_name("FILE")
                .help("Sets the model file path")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("keyword-file-path")
                .short("k")
                .long("keyword-file-path")
                .value_name("FILE")
                .help("Sets the keyword file path(s)")
                .takes_value(true)
                .multiple(true),
        )
        .get_matches();
    let model_file_path = matches.value_of("model-file-path");
    if model_file_path.is_none() {
        println!("Must provide path to the model file!");
        return;
    }
    let keyword_file_paths = matches.values_of("keyword-file-path");
    if keyword_file_paths.is_none() {
        println!("Must provide paths to the keyword file(s)!");
        return;
    }

    // Set model file path
    let model_file_path = model_file_path.unwrap();

    // Set keyword file paths
    let keyword_file_paths: Vec<String> = keyword_file_paths
        .unwrap()
        .map(|val| val.to_string())
        .collect();

    // Set sensitivities
    let sensitivities: Vec<f32> = keyword_file_paths.iter().map(|_val| 0.5).collect();

    // Setup microphone
    let device = cpal::default_input_device().expect("Failed to get default input device");
    let format = device
        .default_input_format()
        .expect("Failed to get default input format");
    let event_loop = Arc::new(cpal::EventLoop::new());
    let stream_id = event_loop
        .build_input_stream(&device, &format)
        .expect("Failed to build input stream");
    event_loop.play_stream(stream_id.clone());

    // Print microphone information
    println!("Default input device: {}", device.name());
    println!("Default input format: {:?}", format);

    // Setup OS signals
    let signal = chan_signal::notify(&[chan_signal::Signal::INT, chan_signal::Signal::TERM]);

    // Setup Porcupine
    let frame_length = unsafe { pv::frame_length() };
    let object = if keyword_file_paths.len() == 1 {
        unsafe {
            pv::Object::new(&model_file_path, &keyword_file_paths[0], sensitivities[0]).unwrap()
        }
    } else {
        let keyword_file_paths: Vec<&str> = keyword_file_paths.iter().map(|s| s.as_str()).collect();
        unsafe {
            pv::Object::new_multiple_keywords(&model_file_path, &keyword_file_paths, &sensitivities)
                .unwrap()
        }
    };

    // Start the microphone and detect keyword
    let recording = Arc::new(AtomicBool::new(true));
    let recording_1 = recording.clone();
    let event_loop_1 = event_loop.clone();
    thread::spawn(move || {
        let mut samples_buffer = Vec::new();
        let mut last_detection_time = SystemTime::now();

        // Run the microphone event loop
        event_loop_1.run(move |_, data| {
            // If we're done recording, return early.
            if !recording_1.load(Ordering::Relaxed) {
                return;
            }

            // Create a closure for detecting keyword
            let mut detect_keyword = |samples_buffer: &mut Vec<i16>,
                                      samples: Vec<i16>,
                                      sample_rate: u32,
                                      channels: u16| {
                // Read the interleaved samples and convert them to a signal.
                if channels == 2 {
                    let signal = sample::signal::from_interleaved_samples_iter::<_, [i16; 2]>(
                        samples.iter().cloned(),
                    );

                    // Convert the signal's sample rate using `Sinc` interpolation.
                    let ring_buffer = sample::ring_buffer::Fixed::from([[0i16, 0i16]; 100]);
                    let sinc = sample::interpolate::Sinc::new(ring_buffer);
                    let new_signal = sample::interpolate::Converter::from_hz_to_hz(
                        signal,
                        sinc,
                        sample_rate as f64,
                        16000 as f64,
                    );

                    // Add audio data to the buffer
                    let new_samples: Vec<i16> =
                        new_signal.until_exhausted().map(|frame| frame[0]).collect();
                    samples_buffer.extend_from_slice(&new_samples);
                } else {
                    let signal = sample::signal::from_interleaved_samples_iter::<_, [i16; 1]>(
                        samples.iter().cloned(),
                    );

                    // Convert the signal's sample rate using `Sinc` interpolation.
                    let ring_buffer = sample::ring_buffer::Fixed::from([[0i16]; 100]);
                    let sinc = sample::interpolate::Sinc::new(ring_buffer);
                    let new_signal = sample::interpolate::Converter::from_hz_to_hz(
                        signal,
                        sinc,
                        sample_rate as f64,
                        16000 as f64,
                    );

                    // Add audio data to the buffer
                    let new_samples: Vec<i16> =
                        new_signal.until_exhausted().map(|frame| frame[0]).collect();
                    samples_buffer.extend_from_slice(&new_samples);
                }

                // Process buffer when it reaches Porcupine frame length
                if samples_buffer.len() >= frame_length {
                    let data: Vec<i16> = samples_buffer.drain(..frame_length).collect();
                    if keyword_file_paths.len() == 1 {
                        if let Ok(detected) = unsafe { object.process(&data) } {
                            if detected {
                                println!("Detected keyword!");
                            }
                        }
                    } else {
                        if let Ok(detected) = unsafe { object.process_multiple_keywords(&data) } {
                            if detected >= 0 {
                                if SystemTime::now().duration_since(last_detection_time).unwrap().as_secs() < 1 {
                                    return;
                                }
                                match detected {
                                    0 => {
                                        tx.send(TOGGLE).unwrap();
                                        println!("Toggle");
                                    },
                                    1 => {
                                        tx.send(POWER_OFF).unwrap();
                                        println!("Power off");
                                    },
                                    2 => {
                                        tx.send(POWER_ON).unwrap();
                                        println!("Power on");
                                    },
                                    4 => {
                                        tx.send(TOO_COLD).unwrap();
                                        println!("Too cold");
                                    },
                                    3 => {
                                        tx.send(TOO_HOT).unwrap();
                                        println!("Too hot");
                                    },
                                    _ => println!("Unknown command"),
                                }
                                last_detection_time = SystemTime::now();
                            }
                        }
                    }
                }
            };

            // Otherwise process audio for keyword detection
            match data {
                cpal::StreamData::Input {
                    buffer: cpal::UnknownTypeInputBuffer::I16(buffer),
                } => {
                    let samples: Vec<i16> = buffer.iter().cloned().collect();

                    detect_keyword(
                        &mut samples_buffer,
                        samples,
                        format.sample_rate.0,
                        format.channels,
                    );
                }
                cpal::StreamData::Input {
                    buffer: cpal::UnknownTypeInputBuffer::F32(buffer),
                } => {
                    let samples: Vec<f32> = buffer.iter().cloned().collect();
                    let samples: Vec<i16> = samples
                        .iter()
                        .map(|val| sample::conv::f32::to_i16(*val))
                        .collect();

                    detect_keyword(
                        &mut samples_buffer,
                        samples,
                        format.sample_rate.0,
                        format.channels,
                    );
                }
                cpal::StreamData::Input {
                    buffer: cpal::UnknownTypeInputBuffer::U16(buffer),
                } => {
                    let samples: Vec<u16> = buffer.iter().cloned().collect();
                    let samples: Vec<i16> = samples
                        .iter()
                        .map(|val| sample::conv::u16::to_i16(*val))
                        .collect();

                    detect_keyword(
                        &mut samples_buffer,
                        samples,
                        format.sample_rate.0,
                        format.channels,
                    );
                }
                _ => {}
            }
        });
    });

    // Wait for an OS signal
    chan_select! {
        signal.recv() -> signal => {
            println!("received signal: {:?}", signal);
            recording.store(false, Ordering::Relaxed);
            event_loop.destroy_stream(stream_id.clone());
        },
    }
}